# Todo
## insert
## volume
widget: pavucontrol
TBD: status bar widget

## amazee.io
## ssh keys
## spotify
## standard window setup

## Automatically mounting drives

# Postponed

## backlight
### Problem 1:
Unable to do this:
https://askubuntu.com/questions/76081/brightness-not-working-after-installing-nvidia-driver

Outdated:
```
$ xbacklight -dec 20
No outputs have backlight property
```
https://askubuntu.com/questions/812734/macbook-pro-backlight-control-not-working-on-ubuntu-16-04
possible alternative to xbacklight: http://haikarainen.github.io/light/

Also try this: https://wiki.debian.org/MacBookPro#NVidia_graphics

Another possible solution: "RegistryDwords" "EnableBrightnessControl=1" in .xorg.conf

And: https://askubuntu.com/questions/264247/proprietary-nvidia-drivers-with-efi-on-mac-to-prevent-overheating/613573

### Problem 2:
XF86MonBrightnessUp and XF86MonBrightnessDown aren't working. Run `xev` to debug the keys.


# Done

## Adding non-free debian
```
nano /etc/apt/sources.list
deb http://httpredir.debian.org/debian/ stretch main contrib
```

## Add user to sudoers
```
usermod -aG sudo luk
```

## Install basic tools
```
sudo apt-get install i3 i3lock i3status xorg dmenu network-manager-gnome htop git pavucontrol pulseaudio nvidia-settings nvidia-driver nvidia-opencl-common xserver-xorg-input-synaptics chromium hfsprogs unzip
exit
startx
```

## Keyboard layout
```
nano .config/i3/config
exec "setxkbmap -layout ch -variant de_mac"
```

## UI font size
```
nano .config/i3/config
```

## Download hyper
```
install as root: dpkg -i ...
sudo apt —fix-broken install
```

## Resolution
https://wiki.archlinux.org/index.php/HiDPI#X_Resources
```
nano .Xresources

Xft.dpi: 180
Xft.autohint: 0
Xft.lcdfilter: lcddefault
Xft.hintstyle: hintfull
Xft.hinting: 1
Xft.antialias: 1
Xft.rgba: rgb
```

## Wifi
```
apt-get install linux-image-$(uname -r|sed 's,[^-]*-[^-]*-,,') linux-headers-$(uname -r|sed 's,[^-]*-[^-]*-,,') broadcom-sta-dkms
modprobe -r b44 b43 b43legacy ssb brcmsmac bcma
modprobe wl
```

## touchpad / right click
Solved by installing synaptics driver: https://packages.debian.org/stretch/xserver-xorg-input-synaptics
And:
https://askubuntu.com/questions/742920/ubuntu-to-mac-ctrlclick-right-click

## audio
working now: pulseaudio

## node/nvm
```
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"# This loads nvm bash_completion
```

## Hyper
```
dpgk -i hyper_2.0.0_amd64.deb
apt --fix-broken install
```

## PhpStorm
```
tar xvzf /home/luk/Downloads/PhpStorm-2018.2.5.tar.gz -C /opt
cd /usr/bin/
sudo ln -s /opt/PhpStorm-182.4892.16/bin/phpstorm.sh .
```

## Start i3
```
startx
```

## Firefox
```
wget https://download-installer.cdn.mozilla.net/pub/devedition/releases/64.0b7/linux-x86_64/en-US/firefox-64.0b7.tar.bz2
tar xjf firefox-64.0b7.tar.bz2
sudo mv firefox /opt/firefox-dev
sudo ln -s /opt/firefox-dev/firefox /usr/bin/firefox
sudo update-alternatives --install /usr/bin/x-www-browser x-www-browser /opt/firefox-dev/firefox 100
```

## Setup multiple screens
```
xrandr --output eDP-1 --off
xrandr --output HDMI-1 --auto --above eDP-1
xrandr --output DP-1 --auto --right-of HDMI-1 --dpi 144
xrandr --output DP-2 --auto --right-of HDMI-1 --dpi 96
```

## Screen tearing bug
All done in the .xorg.conf in this repository.

Source:
First install nvidia drivers (already done earlier): https://www.youtube.com/watch?v=KUsnygrNUMw
Instructions: https://www.youtube.com/watch?v=oYWer86A20s

## @    
Right alt key + g
## tilde
Right alt key + n

## Reboot/Shutdown
```
$ systemctl reboot
$ systemctl poweroff
```

## multiple screens


## Mounting HFS volumes
```
$ sudo fdisk -l
$ sudo mkdir /mnt/data
$ sudo chmod 755 /mnt/data
$ sudo mount /dev/sdc2 /mnt/data
```

## Unmounting drives
`sudo umount /dev/sdc2`

## Unzipping files
`unzip archive.zip -d archive`

# Optional
